from custom_math import add,sub,mul

assert(add(1,1) == 2)
assert(add(2,1) == 3)
assert(sub(1,1) == 0)
assert(sub(1,0) == 1)
assert(mul(1,2) == 2)
assert(mul(2,2) == 4)